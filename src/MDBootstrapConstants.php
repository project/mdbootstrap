<?php

namespace Drupal\mdbootstrap;

/**
 * Defines constants for the Material Design Bootstrap module.
 */
class MDBootstrapConstants {

  /**
   * The latest version of Material Design Bootstrap UI KIT.
   */
  const MDBOOTSTRAP_LATEST_VERSION = '7.3.1';

}
