<?php

namespace Drupal\mdbootstrap\Form;

use Drupal\bootstrap_ui\Form\BootstrapSettings;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mdbootstrap\MDBootstrapConstants;

/**
 * Defines a form that configures Material Design Bootstrap settings.
 */
class MDBootstrapSettings extends BootstrapSettings {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Latest version of the Material Design Bootstrap UI Kit.
    $latest_version = MDBootstrapConstants::MDBOOTSTRAP_LATEST_VERSION;

    // Flags for Material Design Bootstrap version.
    $free_label = ' <span class="bootstrap-free-flag">Free</span>';
    $pro_label = ' <span class="bootstrap-pro-flag">Pro</span>';

    // The Bootstrap UI config settings.
    $configBootstrap = $this->config('bootstrap_ui.settings');
    $config = $this->config('mdbootstrap.settings');

    $mdb_releases = mdbootstrap_version_releases();
    $version_states = [];
    foreach ($mdb_releases["MD Bootstrap v4"] as $version) {
      $version_states[] = ['select[name="version"]' => ['value' => $version]];
    }
    $form['options_wrapper']['dark'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Dark mode'),
      '#default_value' => $config->get('dark'),
      '#description'   => $this->t('To create a dark mode toggle button enable dark mode styles.'),
      '#states' => [
        'visible' => [
          'select[name="library"]' => ['value' => 'mdbootstrap'],
        ],
        'invisible' => $version_states,
      ],
    ];

    $form['options_wrapper']['lite'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Lite version'),
      '#default_value' => $config->get('lite'),
      '#description'   => $this->t('Use lite version if you don’t plan to use MD Bootstrap components to save a some kilobytes.'),
      '#states' => [
        'visible' => [
          'select[name="library"]' => ['value' => 'mdbootstrap'],

        ],
        'visible' => $version_states,
      ],
    ];

    $is_mdbootstrap = is_null($form_state->getValue('library')) ? $configBootstrap->get('library') == 'mdbootstrap' : $form_state->getValue('library') == 'mdbootstrap';
    if ($is_mdbootstrap) {
      // Show warning missing library and lock on cdn method.
      $method = $configBootstrap->get('method');
      $method_lock_change = FALSE;
      if (mdbootstrap_check_installed() === FALSE) {
        $method = 'cdn';
        $method_lock_change = TRUE;
        $method_warning = $this->t('You cannot set local due to the MDBootstrap library is missing. Please <a href=":downloadUrl" rel="external" target="_blank">Download</a> and extract to <strong>"/libraries/mdb-ui-kit"</strong> or <strong>"/libraries/mdbootstrap"</strong> directory.', [
          ':downloadUrl' => 'https://github.com/mdbootstrap/mdb-ui-kit/tags',
        ]);

        // Display warning message off.
        $form['hide'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Hide warning'),
          '#default_value' => $configBootstrap->get('hide') ?? FALSE,
          '#description'   => $this->t("If you want to use the CDN without installing the local library, you can turn off the warning."),
          '#weight'        => -12,
        ];
        $form['method_warning'] = [
          '#type'   => 'item',
          '#markup' => '<div class="library-status-report">' . $method_warning . '</div>',
          '#prefix' => '<div id="method-warning">',
          '#suffix' => '</div>',
          '#states' => [
            'visible' => [
              ':input[name="load"]' => ['checked' => TRUE],
            ],
            'invisible' => [
              ':input[name="hide"]' => ['checked' => TRUE],
            ],
          ],
          '#weight' => -9,
        ];

        $version = !empty($form_state->getValue('version')) ? $form_state->getValue('version') : (!empty($config->get('version')) ? $config->get('version') : $latest_version);
      }
      else {
        $version = ($method == 'cdn') ? (!empty($form_state->getValue('version')) ? $form_state->getValue('version') : $config->get('version')) : mdbootstrap_detect_version();
      }

      $form['method']['#default_value'] = $method;
      $form['method']['#disabled'] = $method_lock_change;

      // Get MD Bootstrap version type.
      $type = mdbootstrap_detect_type() == 'FREE' ? $free_label : $pro_label;

      // Get MDBootstrap Stable versions.
      $versions = mdbootstrap_version_options();
      $in_versions = in_array($version, _bootstrap_ui_array_flatten($versions));
      $form['version_wrapper']['version']['#options'] = $versions;
      $form['version_wrapper']['version']['#default_value'] = $in_versions ? $version : 'other';

      // Get MDBootstrap latest stable releases.
      $releases = mdbootstrap_version_releases();
      $in_releases = in_array($version, _bootstrap_ui_array_flatten($releases));
      $form['version_wrapper']['release']['#options'] = $in_releases ? $releases : $releases + ['' => $version . ' (' . $this->t('Not stable') . ')'];
      $form['version_wrapper']['release']['#default_value'] = $in_releases ? $version : '';

      $form['version_wrapper']['assets']['#field_suffix'] = $type;

      // Always set variant to minimized version for MD Bootstrap.
      $form['build']['variant']['#value'] = 1;
      $form['build']['variant']['#default_value'] = 1;

      $triggerdElement = $form_state->getTriggeringElement();
      if (isset($triggerdElement['#name']) && ($triggerdElement['#name'] == 'version' || $triggerdElement['#name'] == 'release')) {
        $current_version = $triggerdElement['#value'] != 'other' ? $triggerdElement['#value'] : $latest_version;
      }
      else {
        $current_version = $version;
      }

      // States for JavaScript and CSS files.
      $file_states = [
        ':input[name="js"]' => ['checked' => FALSE],
        ':input[name="css"]' => ['checked' => FALSE],
      ];

      // Main MD Bootstrap JavaScript file.
      $form['file_types']['js'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Attach JavaScript'),
        '#default_value' => $config->get('file_types.js.enabled') ?? TRUE,
        '#description'   => $this->t('If you don’t plan to use MD Bootstrap components, you can disable the JavaScript file to save a some kilobytes.'),
        '#prefix'        => '<div id="file-js">',
        '#suffix'        => '</div>',
      ];

      // MD Bootstrap 4 JavaScript files.
      if (intval($current_version) == 4) {
        // Add MDB 4 JavaScript files to states.
        $file_states['js_bootstrap'] = ['checked' => FALSE];
        $file_states['js_mdbootstrap'] = ['checked' => FALSE];
        $file_states['js_popper'] = ['checked' => FALSE];

        // Particle JS files.
        $form['file_types']['js_files'] = [
          '#type'       => 'container',
          '#title'      => $this->t('Particle JS files'),
          '#prefix'     => '<div id="file-js">',
          '#suffix'     => '</div>',
          '#states'     => [
            'invisible' => [
              ':input[name="js"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $form['file_types']['js_files']['js_bootstrap'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use Bootstrap'),
          '#default_value' => $config->get('file_types.js.bootstrap'),
        ];
        $form['file_types']['js_files']['js_mdbootstrap'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use MDBootstrap'),
          '#default_value' => $config->get('file_types.js.mdbootstrap'),
        ];
        $form['file_types']['js_files']['js_popper'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use Popper'),
          '#default_value' => $config->get('file_types.js.popper'),
        ];
      }

      // Main MD Bootstrap CSS file.
      $form['file_types']['css'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Attach CSS'),
        '#default_value' => $config->get('file_types.css.enabled') ?? TRUE,
        '#description'   => $this->t('If you don’t plan to use Bootstrap css or just you want to use specific files such as reset file, you can disable the Attach CSS to see and select separated files.'),
      ];

      // MD Bootstrap 4 CSS files.
      if (intval($current_version) == 4) {
        // Add MDB 4 CSS files to states.
        $file_states['css_fontawesome'] = ['checked' => FALSE];
        $file_states['css_googlefonts'] = ['checked' => FALSE];
        $file_states['css_bootstrap'] = ['checked' => FALSE];
        $file_states['css_mdbootstrap'] = ['checked' => FALSE];

        // Particle CSS files.
        $form['file_types']['css_files'] = [
          '#type'       => 'container',
          '#title'      => $this->t('Particle CSS files'),
          '#prefix'     => '<div id="file-css">',
          '#suffix'     => '</div>',
          '#states'     => [
            'invisible' => [
              ':input[name="css"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $form['file_types']['css_files']['css_fontawesome'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use Font Awesome'),
          '#default_value' => $config->get('file_types.css.fontawesome'),
        ];
        $form['file_types']['css_files']['css_googlefonts'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use Google fonts'),
          '#default_value' => $config->get('file_types.css.googlefonts'),
        ];
        $form['file_types']['css_files']['css_bootstrap'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use Bootstrap'),
          '#default_value' => $config->get('file_types.css.bootstrap'),
        ];
        $form['file_types']['css_files']['css_mdbootstrap'] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Use MDBootstrap'),
          '#default_value' => $config->get('file_types.css.mdbootstrap'),
        ];
      }

      // Warning for disabled all files.
      $file_warning = $this->t('If all JavaScript and CSS files are disabled, the library cannot load. Therefore, after saving settings, the load option will be disabled and revert to the default state.');
      $form['file_types']['file_warning'] = [
        '#type'   => 'item',
        '#markup' => '<div class="file-type-report">' . $file_warning . '</div>',
        '#prefix' => '<div id="file-warning">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [
            $file_states,
          ],
        ],
        '#weight' => -9,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['library'] == 'mdbootstrap') {
      // Force detected version from installed library.
      if (mdbootstrap_check_installed() !== FALSE && $values['method'] == 'local') {
        $version = mdbootstrap_detect_version();
      }
      else {
        $version = $values['version'] == 'other' ? $values['release'] : $values['version'];
      }

      // MD Bootstrap dark mode.
      $dark = $values['dark'];
      if (intval($version) < 5) {
        $dark = FALSE;
      }

      // MD Bootstrap lite version.
      $lite = $values['lite'];
      if (intval($version) > 4) {
        $lite = FALSE;
      }

      // Check if no file (JS/CSS) enabled to load.
      if ($values['js'] == 0 && $values['css'] == 0) {
        $js_particles = FALSE;
        $css_particles = FALSE;
        if (intval($version) == 4) {
          $js_particles = (isset($values['js_bootstrap']) && $values['js_bootstrap']) || (isset($values['js_mdbootstrap']) && $values['js_mdbootstrap']) || (isset($values['js_popper']) && $values['js_popper']) ?? FALSE;
          $css_particles = (isset($values['css_bootstrap']) && $values['css_bootstrap']) || (isset($values['css_mdbootstrap']) && $values['css_mdbootstrap']) ?? FALSE;
        }

        // Turn off bootstrap library load instead and reset files to default.
        if (!$js_particles && !$css_particles) {
          $values['js'] = 1;
          $values['css'] = 1;
          $form_state->setValue('load', 0);
        }
      }

      // Save the updated Bootstrap settings.
      $this->config('mdbootstrap.settings')
        ->set('version', $version)
        ->set('dark', $dark)
        ->set('lite', $lite)
        ->set('file_types.js.enabled', $values['js'])
        ->set('file_types.js.bootstrap', isset($values['js_bootstrap']) && $values['js_bootstrap'] !== 0 ?? FALSE)
        ->set('file_types.js.mdbootstrap', isset($values['js_mdbootstrap']) && $values['js_mdbootstrap'] !== 0 ?? FALSE)
        ->set('file_types.js.popper', isset($values['js_popper']) && $values['js_popper'] !== 0 ?? FALSE)
        ->set('file_types.css.enabled', $values['css'])
        ->set('file_types.css.fontawesome', isset($values['css_fontawesome']) && $values['css_fontawesome'] !== 0 ?? FALSE)
        ->set('file_types.css.googlefonts', isset($values['css_googlefonts']) && $values['css_googlefonts'] !== 0 ?? FALSE)
        ->set('file_types.css.bootstrap', isset($values['css_bootstrap']) && $values['css_bootstrap'] !== 0 ?? FALSE)
        ->set('file_types.css.mdbootstrap', isset($values['css_mdbootstrap']) && $values['css_mdbootstrap'] !== 0 ?? FALSE)
        ->save();

      // Flush caches so the updated config can be checked.
      drupal_flush_all_caches();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mdbootstrap.settings',
      'bootstrap_ui.settings',
    ];
  }

}
