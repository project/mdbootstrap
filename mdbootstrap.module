<?php

/**
 * @file
 * Drupal`s integration with Material Design Bootstrap UI kits.
 *
 * Bootstrap 5 UI KIT - 700+ components, plain JavaScript,
 * MIT license, simple installation.
 *
 * MDB is a collection of free Top quality open-source
 * UI Kits design tools & resources for JavaScript, Bootstrap,
 * Angular, React & Vue.
 *
 * GitHub:  https://github.com/mdbootstrap/mdb-ui-kit
 * Website: https://mdbootstrap.com
 * license: MIT licensed
 *
 * © 2023 Copyright: MDBootstrap.com
 */

use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function mdbootstrap_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.mdbootstrap':
      $output  = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('MDBootstrap is an innovative and powerful Drupal module that provides professional settings, enabling you to effectively utilize the powerful Material Design UI Kit for Bootstrap framework in your site themes. MDBootstrap is a Top quality open-source Bootstrap 5 UI Kits a collection of templates, over 700 components, plain JavaScript and resources. This module offers a wide range of features and customization options, enhancing your ability to seamlessly integrate and manage Bootstrap within your Drupal user interfaces.') . '</p>';
      $output .= '<h3>' . t('Usage') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt><strong>' . t('Installing MDBootstrap UI Kit') . '</strong></dt>';
      $output .= '<dd><p>' . t('Download the desired version of the MDB UI Kit from GitHub <a target="_blank" href=":mdbootstrap_download">here</a>.', [':mdbootstrap_download' => 'https://github.com/mdbootstrap/mdb-ui-kit/releases']) . '</p></dd>';
      $output .= '<dd><p>' . t('Create a libraries directory in the root if it doesn\'t already exist, and then create a "mdb-ui-kit" directory inside it. The path should be "/libraries/mdb-ui-kit".') . '</p></dd>';
      $output .= '<dd><p>' . t('Place the required files in the /libraries/mdb-ui-kit directory. For the MDBootstrap v5 download (Compiled CSS and JS), the structure should be:') . '</p>';
      $output .= '<pre><code>';
      $output .= '- /libraries/mdb-ui-kit/css/mdb.min.css' . "\n";
      $output .= '- /libraries/mdb-ui-kit/js/mdb.umd.min.js' . "\n";
      $output .= '</code></pre></dd>';
      $output .= '<dd><p>' . t('For MDBootstrap v4 files create a "mdbootstrap" directory inside /libraries, the structure should be:') . '</p>';
      $output .= '<pre><code>';
      $output .= '- /libraries/mdbootstrap/css/mdb.min.css' . "\n";
      $output .= '- /libraries/mdbootstrap/js/mdb.min.js' . "\n";
      $output .= '</code></pre></dd>';
      $output .= '<dd><p>' . t('Note: If both library names, "mdb-ui-kit" and "mdbootstrap", exist in your /libraries path, "mdb-ui-kit" (MDB v5) will be prioritized automatically, and the "mdbootstrap" library will be ignored.') . '</p></dd>';
      $output .= '<br>';
      $output .= '<dt><strong>' . t('MDBootstrap settings page') . '</strong></dt>';
      $output .= '<dd><p>' . t('Go to the <a href=":bootstrap_settings">Bootstrap UI</a> configuration page in your Drupal admin menu.', [':bootstrap_settings' => Url::fromRoute('bootstrap.settings')->setAbsolute()->toString()]) . '</p></dd>';
      $output .= '<dd><p>' . t('Switch the "Library" option from Bootstrap to MDBootstrap to use the Material Design UI Kit across your site, then save the configuration.') . '</p></dd>';
      $output .= '<dd><p>' . t('You can change the other default settings if needed, such as:') . '</p></dd>';
      $output .= '<div><ul>';
      $output .= '<li><strong>' . t('Load MDBootstrap') . '</strong><br><em>' . t('If enabled, this module will attempt to load the MDBootstrap UI Kit for your site.') . '</em></li>';
      $output .= '<li><strong>' . t('Support RTL') . '</strong><br><em>' . t('If the default language or one of the languages of your site is RTL (right-to-left), such as Persian or Arabic, enable this option to support RTL for the MDBootstrap theme. Note: This module automatically uses the RTL version of MDBootstrap or an RTL patch (for older Bootstrap versions) only on pages that require RTL support.') . '</em></li>';
      $output .= '<li><strong>' . t('Hide warning') . '</strong><br><em>' . t('Enable this option to turn off the reminder warning "MDBootstrap library is missing" if you prefer to use the CDN option instead.') . '</em></li>';
      $output .= '<li><strong>' . t('Dark Mode') . '</strong><br><em>' . t('Enable Dark Mode in MDB-UI-KIT through the MDBootstrap module settings for a sleek, dark-themed site.') . '</em></li>';
      $output .= '<li><strong>' . t('Attach method') . '</strong><br><em>' . t('This option allows you to choose whether to load MDBootstrap on your site pages using the locally installed library or the CDN. If the local library is not installed, this option will be disabled and will default to using the CDN. Note: After installing the MDBootstrap framework, remember to set this option to "Local" to use the local libraries instead of the CDN.') . '</em></li>';
      $output .= '<li><strong>' . t('Version') . '</strong><br><em>' . t('The version selection option is only active when using the CDN mode. You can choose the main MDBootstrap versions or select "Other Versions" to use previously released versions. Note: When using the Local mode, the installed library version is automatically detected.') . '</em></li>';
      $output .= '<li><strong>' . t('Build variant') . '</strong><br><em>' . t('Select whether to use the library in Production or Development mode. This lets you choose between the non-minimized (Development) and minimized (Production) versions. These settings work with both local and CDN options.') . '</em></li>';
      $output .= '<li><strong>' . t('Usability') . '</strong><br><em>' . t('Use this section to configure the desired files for specific themes or assign them to specified pages. For example, you can choose to use only the CSS files without loading the JavaScript files, or use only the MDBootstrap Grid CSS for particular cases.') . '</em></li>';
      $output .= '</ul></div>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Check to make sure that Material Design Bootstrap library is installed.
 *
 * This function checks for the presence of the CSS or JS files
 * of the Material Design Bootstrap (MDBootstrap) library to
 * confirm it is properly installed.
 *
 * @return bool
 *   Flag indicating if the library is properly installed.
 */
function mdbootstrap_check_installed() {
  // Initialize the MDBootstrap installation status.
  $mdb_exist = FALSE;
  $file_path = _mdbootstrap_build_manual_file_path();

  // Check for the presence of 'mdb.min.css' in the 'dist' directory.
  if (!empty($file_path) && file_exists($file_path)) {
    $mdb_exist = TRUE;
  }

  return $mdb_exist;
}

/**
 * Identify Material Design Bootstrap installed library version.
 */
function mdbootstrap_detect_version() {
  // Detect if the MDBootstrap library is installed,
  // Return an empty string if the library does not exist.
  if (!mdbootstrap_check_installed()) {
    return '';
  }

  // Find the file path of the MDBootstrap library.
  $file_path = _mdbootstrap_build_manual_file_path();

  // Check if the mdb.min.css file exists and read its content.
  if (!empty($file_path) && file_exists($file_path)) {
    $lines = file($file_path);
    foreach ($lines as $line) {
      if (stripos($line, 'version') !== FALSE) {
        $version = [];
        // Use a regex to match and extract the MDBootstrap version.
        preg_match("/(?:version: )?(?:PRO |FREE |MDB PRO |MDB FREE )?\s*((?:[0-9]+\.?)+)/i", $line, $version);
        if (!empty($version[1])) {
          return $version[1];
        }
      }
    }
  }

  // Return NULL if the version cannot be detected.
  return NULL;
}

/**
 * Identify if Material Design Bootstrap installed library is FREE or PRO.
 */
function mdbootstrap_detect_type() {
  // Return FREE if library not exists for CDN.
  if (!mdbootstrap_check_installed()) {
    return 'FREE';
  }

  // Find the file path of the MDBootstrap library.
  $file_path = _mdbootstrap_build_manual_file_path();

  // Check for the presence of 'mdb.min.css' version type.
  if (file_exists($file_path)) {
    $lines = file($file_path);
    foreach ($lines as $line) {
      if (stripos($line, 'version') !== FALSE) {
        if (stripos($line, 'PRO') !== FALSE) {
          return 'PRO';
        }
        elseif (stripos($line, 'FREE') !== FALSE) {
          return 'FREE';
        }
      }
    }
  }

  // Return FREE if the version type cannot be detected.
  return 'FREE';
}

/**
 * Implements hook_page_attachments().
 *
 * Use Libraries API to load the js & css files into header.
 */
function mdbootstrap_page_attachments(array &$attachments) {
  // Don't add the JavaScript and CSS during installation.
  if (InstallerKernel::installationAttempted()) {
    return;
  }

  // Load the Bootstrap UI configuration settings.
  $configBootstrap = \Drupal::config('bootstrap_ui.settings');

  // Don't add Material Design Bootstrap on specified paths or themes,
  // Bootstrap library settings is not configured to use "MDB" UI kits.
  if (!$configBootstrap->get('load') || $configBootstrap->get('library') != 'mdbootstrap') {
    return;
  }

  // Don't add the JavaScript and CSS on specified paths or themes.
  if (!_bootstrap_ui_check_theme() || !_bootstrap_ui_check_path()) {
    return;
  }

  // Load the Material Design Bootstrap module settings.
  $config = \Drupal::config('mdbootstrap.settings');
  $exists = mdbootstrap_check_installed();

  // Get MDBootstrap with chosen configuration from user settings.
  $version = $config->get('version');
  $methods = $exists !== FALSE ? $configBootstrap->get('method') : 'cdn';

  // Detect local library version automatically when
  // method is not CDN and library existed in root libraries.
  if ($exists && (empty($version) || $version != bootstrap_ui_detect_version()) && $methods != 'cdn') {
    $version = mdbootstrap_detect_version();
    \Drupal::configFactory()->getEditable('mdbootstrap.settings')->set('version', $version)->save();
  }

  // Check language direction to attach Bootstrap RTL patch
  // to support Persian, Farsi or Arabic language characters.
  $language_direction = \Drupal::languageManager()->getCurrentLanguage()->getDirection();
  $rtl = $language_direction == 'rtl' && $configBootstrap->get('rtl') ? '.rtl' : '';

  // Handle MD Bootstrap 5 dark mode.
  $dark = $config->get('dark') ? '.dark' : '';

  // Load Bootstrap library now.
  if ($methods == 'cdn') {
    // Attach MDB UI kits from CDN with variant configuration.
    if (intval($version) >= 5) {
      // Create dynamic discovery library address from configuration.
      $attachments['#attached']['library'][] = 'mdbootstrap/mdb.cdn' . $dark . $rtl;
    }
    else {
      // Support RTL with patch extended by Bootstrap UI module.
      if (!empty($rtl)) {
        $attachments['#attached']['library'][] = 'bootstrap_ui/bootstrap.cdn.rtl';

        // Add JavaScript patch for Bootstrap version 4.x only.
        if (intval($version) == 4) {
          $attachments['#attached']['library'][] = 'bootstrap_ui/bootstrap.patch';
        }
      }

      // Create dynamic discovery library address from configuration
      // with separate RTL support.
      $attachments['#attached']['library'][] = 'mdbootstrap/mdb.cdn' . $dark;
    }
  }
  else {
    // Attach bootstrap from local libraries with variant configuration.
    if (intval($version) >= 5) {
      // Create dynamic discovery library address from configuration.
      $attachments['#attached']['library'][] = 'mdbootstrap/mdb' . $dark . $rtl;
    }
    else {
      // Create dynamic discovery library address from configuration
      // with separate RTL support.
      $attachments['#attached']['library'][] = 'mdbootstrap/mdb' . $rtl;

      // Support RTL with patch extended by Bootstrap UI module.
      if (!empty($rtl)) {
        // Add JavaScript patch for Bootstrap version 4.x only.
        if (intval($version) == 4) {
          $attachments['#attached']['library'][] = 'bootstrap_ui/bootstrap.patch';
        }
      }
    }
  }

}

/**
 * Implements hook_bootstrap_ui_library_names().
 */
function mdbootstrap_bootstrap_ui_library_name($library_name) {
  // Define MD Bootstrap library.
  $library_name['mdbootstrap'] = t('MD Bootstrap');
  return $library_name;
}

/**
 * Provides a list of all MD Bootstrap version releases.
 *
 * This function returns an array of all available version releases
 * of MD Bootstrap categorized by major versions.
 *
 * @return array
 *   An associative array of MD Bootstrap version releases.
 */
function mdbootstrap_version_releases() {
  return [
    'MD Bootstrap v5' => [
      '7.3.1' => '7.3.1',
      '7.3.0' => '7.3.0',
      '7.2.0' => '7.2.0',
      '7.1.0' => '7.1.0',
      '7.0.0' => '7.0.0',
      '6.4.2' => '6.4.2',
      '6.4.1' => '6.4.1',
      '6.4.0' => '6.4.0',
      '6.3.1' => '6.3.1',
      '6.3.0' => '6.3.0',
      '6.2.0' => '6.2.0',
      '6.1.0' => '6.1.0',
      '6.0.0' => '6.0.0',
      '5.0.0' => '5.0.0',
    ],
    'MD Bootstrap v4' => [
      '4.20.0' => '4.20.0',
      '4.19.2' => '4.19.2',
      '4.19.1' => '4.19.1',
      '4.19.0' => '4.19.0',
      '4.18.0' => '4.18.0',
      '4.17.0' => '4.17.0',
      '4.16.0' => '4.16.0',
      '4.15.0' => '4.15.0',
      '4.14.1' => '4.14.1',
      '4.14.0' => '4.14.0',
      '4.13.0' => '4.13.0',
      '4.12.0' => '4.12.0',
      '4.11.0' => '4.11.0',
      '4.10.1' => '4.10.1',
      '4.10.0' => '4.10.0',
      '4.9.0' => '4.9.0',
    ],
  ];
}

/**
 * Provides a list of MD Bootstrap version options for a form element.
 *
 * This function returns an array of version options for MD Bootstrap
 * to be used in form elements, categorized by major versions.
 *
 * @return array
 *   An associative array of MD Bootstrap version options for forms.
 */
function mdbootstrap_version_options() {
  return [
    'MD Bootstrap v5' => [
      '7.3.1' => '7.3.1',
      '7.2.0' => '7.2.0',
      '7.1.0' => '7.1.0',
      '7.0.0' => '7.0.0',
      '6.4.2' => '6.4.2',
      '6.3.1' => '6.3.1',
      '6.2.0' => '6.2.0',
    ],
    'MD Bootstrap v4' => [
      '4.20.0' => '4.20.0',
      '4.19.2' => '4.19.2',
      '4.18.0' => '4.18.0',
    ],
    'All releases' => [
      'other' => t('Other versions'),
    ],
  ];
}

/**
 * Implements hook_library_info_alter().
 */
function mdbootstrap_library_info_alter(&$libraries, $extension) {
  if ($extension === 'mdbootstrap') {

    // Check first the Bootstrap library if not MD Bootstrap.
    $configBootstrap = \Drupal::config('bootstrap_ui.settings');
    if ($configBootstrap->get('library') != 'mdbootstrap') {
      return;
    }

    // Load MD Bootstrap settings configuration.
    $configs = \Drupal::config('mdbootstrap.settings');
    $version = $configs->get('version');

    $file_types = $configs->get('file_types');

    if (!empty($version)) {
      $remote_bs5 = 'https://github.com/mdbootstrap/mdb-ui-kit/archive/' . $version . '.zip';
      $remote_bs4 = 'https://github.com/mdbootstrap/material-design-for-bootstrap/archive/' . $version . '.zip';

      // Set remote download URL by current version.
      foreach ($libraries as $name => $library) {
        if (isset($library['remote'])) {
          $libraries[$name]['remote'] = intval($version) == 4 ? $remote_bs4 : $remote_bs5;
        }
        // Skip local for library.
        else {
          continue;
        }
        if (isset($library['version'])) {
          $libraries[$name]['version'] = $version;
        }

        $external = FALSE;
        $file_url = '/libraries/mdb-ui-kit';
        $css_type = $file_types['css']['enabled'];
        $js_types = $file_types['js']['enabled'];

        // Get possible library installed paths.
        $mdb_path = bootstrap_ui_find_library('mdb');
        $mdbootstrap_path = bootstrap_ui_find_library('mdbootstrap');
        $mdb_ui_kit_path = bootstrap_ui_find_library('mdb-ui-kit');

        // Check if each library path exists and override the file_url
        // prioritizing in order if multiple paths exist simultaneously.
        if ($mdb_ui_kit_path) {
          $file_url = '/' . $mdb_ui_kit_path;
        }
        elseif ($mdbootstrap_path) {
          $file_url = '/' . $mdbootstrap_path;
        }
        elseif ($mdb_path) {
          $file_url = '/' . $mdb_path;
        }

        // Modify MDBootstrap CDN library by selected version.
        if (strpos($name, 'cdn') !== FALSE) {
          // Set CDN remote URL.
          $libraries[$name]['remote'] = '//cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/' . $version;

          // Default CDN of Material Design for Bootstrap version 5.
          $file_url = $libraries[$name]['remote'];
          $external = TRUE;

          // Build dynamic URL of the cdnjs CDN from chosen version.
          $css_file = $file_url . (strpos($name, 'dark') !== FALSE ? '/mdb.dark.min.css' : '/mdb.min.css');
          $rtl_file = $file_url . (strpos($name, 'dark') !== FALSE ? '/mdb.dark.rtl.min.css' : '/mdb.rtl.min.css');
          $js_file = $file_url . (intval($version) == 7 ? '/mdb.umd.min.js' : '/mdb.min.js');

          if (intval($version) == 4) {
            $libraries[$name]['remote'] = '//cdnjs.cloudflare.com/ajax/libs/mdbootstrap/' . $version;

            // Default CDN of Material Design for Bootstrap version 4.
            $file_url = $libraries[$name]['remote'];

            // Build dynamic URL of the cdnjs CDN from chosen version.
            $css_file = $file_url . ($configs->get('lite') ? '/css/mdb.lite.min.css' : '/css/mdb.min.css');
            $js_file = $file_url . '/js/mdb.min.js';

            $mdBootstrapVersions = [
              '4.20.0' => '4.6.0',
              '4.19.2' => '4.5.0',
              '4.19.1' => '4.5.0',
              '4.19.0' => '4.5.0',
              '4.18.0' => '4.4.1',
              '4.17.0' => '4.4.1',
              '4.16.0' => '4.4.1',
              '4.15.0' => '4.4.1',
              '4.14.1' => '4.4.1',
              '4.14.0' => '4.4.1',
              '4.13.0' => '4.4.1',
              '4.12.0' => '4.4.1',
              '4.11.0' => '4.4.1',
              '4.10.1' => '4.3.1',
              '4.10.0' => '4.3.1',
              '4.9.0'  => '4.3.1',
            ];

            // Default CDN of Bootstrap version 4.
            $bootstrap_cdn_url = '//cdn.jsdelivr.net/npm/bootstrap@' . $mdBootstrapVersions[$version];

            // Build dynamic URL of the jsdelivr CDN from chosen version.
            $bootstrap_css_file = $bootstrap_cdn_url . '/dist/css/bootstrap.min.css';
            $bootstrap_js_files = $bootstrap_cdn_url . '/dist/js/bootstrap.min.js';

            // Font Awesome and Google fonts for Material Design Bootstrap.
            $fontawesome = '//use.fontawesome.com/releases/v5.11.2/css/all.css';
            $googlefonts = '//fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap';

            // Popper (tooltips or popovers) for bootstrap 4.x.
            $popper = '//cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js';

            $css_files = [];
            $js_files = [];
            if ((!$js_types || !$css_type) && intval($version) == 4) {
              $js_particles = $file_types['js']['bootstrap'] || $file_types['js']['mdbootstrap'] || $file_types['js']['popper'] ?? FALSE;
              $js_files['popper'] = $file_types['js']['popper'] ? $popper : NULL;
              $js_files['bootstrap'] = $file_types['js']['bootstrap'] ? $bootstrap_js_files : NULL;
              $js_files['mdbootstrap'] = $file_types['js']['mdbootstrap'] ? $js_file : NULL;

              $css_particles = $file_types['css']['bootstrap'] || $file_types['css']['mdbootstrap'] ?? FALSE;
              $css_files['fontawesome'] = $file_types['css']['fontawesome'] ? $fontawesome : NULL;
              $css_files['googlefonts'] = $file_types['css']['googlefonts'] ? $googlefonts : NULL;
              $css_files['bootstrap'] = $file_types['css']['bootstrap'] ? $bootstrap_css_file : NULL;
              $css_files['mdbootstrap'] = $file_types['css']['mdbootstrap'] ? $css_file : NULL;
            }
          }
        }
        else {
          $css_file = $file_url . (strpos($name, 'dark') !== FALSE ? '/css/mdb.dark.min.css' : '/css/mdb.min.css');
          $rtl_file = $file_url . (strpos($name, 'dark') !== FALSE ? '/css/mdb.dark.rtl.min.css' : '/css/mdb.rtl.min.css');
          $js_file = $file_url . (intval($version) == 7 ? '/js/mdb.umd.min.js' : '/js/mdb.min.js');

          if (intval($version) == 4) {
            // Font Awesome and Google fonts for Material Design Bootstrap.
            $fontawesome = '//use.fontawesome.com/releases/v5.11.2/css/all.css';
            $googlefonts = '//fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap';

            // Build dynamic URL of the cdnjs CDN from chosen version.
            $bootstrap_css_file = $file_url . '/css/bootstrap.min.css';
            $bootstrap_js_files = $file_url . '/js/bootstrap.min.js';

            // Popper (tooltips or popovers) for bootstrap 4.x.
            $popper = $file_url . '/js/popper.min.js';

            $css_file = $file_url . ($configs->get('lite') ? '/css/mdb.lite.min.css' : '/css/mdb.min.css');
            $js_file = $file_url . '/js/mdb.min.js';

            $css_files = [];
            $js_files = [];
            if ((!$js_types || !$css_type)) {
              $js_particles = $file_types['js']['bootstrap'] || $file_types['js']['mdbootstrap'] || $file_types['js']['popper'] ?? FALSE;
              $js_files['popper'] = $file_types['js']['popper'] ? $popper : NULL;
              $js_files['bootstrap'] = $file_types['js']['bootstrap'] ? $bootstrap_js_files : NULL;
              $js_files['mdbootstrap'] = $file_types['js']['mdbootstrap'] ? $js_file : NULL;

              $css_particles = $file_types['css']['bootstrap'] || $file_types['css']['mdbootstrap'] ?? FALSE;
              $css_files['fontawesome'] = $file_types['css']['fontawesome'] ? $fontawesome : NULL;
              $css_files['googlefonts'] = $file_types['css']['googlefonts'] ? $googlefonts : NULL;
              $css_files['bootstrap'] = $file_types['css']['bootstrap'] ? $bootstrap_css_file : NULL;
              $css_files['mdbootstrap'] = $file_types['css']['mdbootstrap'] ? $css_file : NULL;
            }
          }
        }

        // Set CDN for CSS file.
        if (isset($library['css'])) {
          unset($libraries[$name]['css']);

          if ($css_type) {
            if (intval($version) == 4) {
              $libraries[$name]['css']['theme'][$fontawesome] = [
                'type' => 'external',
                'minified' => TRUE,
              ];
              $libraries[$name]['css']['theme'][$googlefonts] = [
                'type' => 'external',
              ];
              $libraries[$name]['css']['theme'][$bootstrap_css_file] = [
                'minified' => TRUE,
              ];
              if ($external) {
                $libraries[$name]['css']['theme'][$bootstrap_css_file]['type'] = 'external';
              }
            }

            $libraries[$name]['css']['theme'][$css_file] = [
              'minified' => TRUE,
            ];
            if ($external) {
              $libraries[$name]['css']['theme'][$css_file]['type'] = 'external';
            }
          }
          elseif (intval($version) == 4 && $css_particles) {
            foreach ($css_files as $css_particle) {
              if ($css_particle) {
                $libraries[$name]['css']['theme'][$css_particle] = [];
                if ($external) {
                  $libraries[$name]['css']['theme'][$css_particle]['type'] = 'external';
                }
              }
            }
          }
        }

        // Set CDN for RTL language CSS file.
        if (intval($version) > 4 && strpos($name, 'rtl') !== FALSE && isset($library['css'])) {
          unset($libraries[$name]['css']);
          if ($css_type) {
            $libraries[$name]['css']['theme'][$rtl_file] = [
              'minified' => TRUE,
            ];
            if ($external) {
              $libraries[$name]['css']['theme'][$rtl_file]['type'] = 'external';
            }
          }
        }

        // Set CDN for JavaScript file.
        if (isset($library['js'])) {
          unset($libraries[$name]['js']);
          if ($js_types) {
            if (intval($version) == 4) {
              $libraries[$name]['js'][$popper] = [
                'type' => 'external',
                'minified' => TRUE,
              ];
              $libraries[$name]['js'][$bootstrap_js_files] = [
                'type' => 'external',
                'minified' => TRUE,
              ];
            }

            $libraries[$name]['js'][$js_file] = [
              'minified' => TRUE,
            ];
            if ($external) {
              $libraries[$name]['js'][$js_file]['type'] = 'external';
            }
          }
          elseif (intval($version) == 4 && $js_particles) {
            foreach ($js_files as $js_particle) {
              if ($js_particle) {
                $libraries[$name]['js'][$js_particle] = [];
                if ($external) {
                  $libraries[$name]['js'][$js_particle]['type'] = 'external';
                }
              }
            }
          }
        }

        // Set CDN for dependencies file (just for sort).
        if (isset($library['dependencies'])) {
          unset($libraries[$name]['dependencies']);
          if ($js_types) {
            $libraries[$name]['dependencies'][] = 'core/jquery';
          }
        }

      }

      // Check language direction to attach Bootstrap RTL patch
      // to support Persian/Arabic language characters.
      if (intval($version) == 4) {
        $patch_csspath = \Drupal::request()->getBasePath() . '/' . \Drupal::service('extension.list.module')->getPath('bootstrap_ui') . '/css/patch/';
        $patch_version = '4.x';
        $patch_current = $patch_csspath . $patch_version . '/bootstrap.rtl.min.css';

        $libraries['mdb.cdn.rtl']['version'] = 'VERSION';
        $libraries['mdb.cdn.rtl']['css']['theme'][$patch_current] = ['minified' => TRUE];

        if (isset($libraries['bootstrap.rtl'])) {
          unset($libraries['bootstrap.rtl']);
        }
        $libraries['mdb.rtl']['version'] = 'VERSION';
        $libraries['mdb.rtl']['css']['theme'][$patch_current] = ['minified' => TRUE];
      }
    }

  }
}

/**
 * Constructs the file path for a specified Material Design Bootstrap file.
 *
 * @return string
 *   Constructed file path based on the provided parameters.
 */
function _mdbootstrap_build_manual_file_path() {
  // Find the path of the Material Design Bootstrap library.
  $mdb_path = bootstrap_ui_find_library('mdb');
  $mdb_ui_kit_path = bootstrap_ui_find_library('mdb-ui-kit');
  $mdbootstrap_path = bootstrap_ui_find_library('mdbootstrap');

  $file_path = '';

  // If the library path is found, proceed to check for the required file.
  // Prioritize the paths in the specified order.
  if ($mdb_ui_kit_path) {
    $file_path = _bootstrap_ui_build_manual_file_path($mdb_ui_kit_path, 'dist', 'mdb');
  }
  elseif ($mdbootstrap_path) {
    $file_path = _bootstrap_ui_build_manual_file_path($mdbootstrap_path, 'dist', 'mdb');
  }
  elseif ($mdb_path) {
    $file_path = _bootstrap_ui_build_manual_file_path($mdb_path, 'dist', 'mdb');
  }

  // Return the appropriate path based on the bootstrap mode.
  return $file_path;
}
