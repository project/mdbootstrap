INTRODUCTION
------------

This module integrates MDBootstrap, Top quality open-source Bootstrap 5 UI Kits
a collection of templates, over 700 components, plain JavaScript and resources.
- https://mdbootstrap.com/


FEATURES
--------

This module aims to provide various settings:

  - Support all stable versions v5 and v4

  - Compatible with Free and Pro Versions

  - Load via CDN method

  - Load from local libraries

  - RTL support for all versions (Such as Persian, Farsi & Arabic languages)

  - Ability to use dark and light version

  - Ability to set minified or non-minified library

  - Restricts load by files, themes and paths

  - Restrict JavaScript and CSS files to be loaded

  - Customizable

  - Easy to use

  - Composer install

  - More possibilities in the future


REQUIREMENTS
------------

Download MDBootstrap UI-Kit Desired Version from GitHub:

  - https://github.com/mdbootstrap/mdb-ui-kit/releases


INSTALLATION
------------

1. Download 'MDBootstrap' module:
   - https://www.drupal.org/project/mdbootstrap

2. Extract and place it in the root of contributed modules directory i.e.
   * /modules/contrib/mdbootstrap

3. Create a libraries directory in the root, if not already there i.e.
   * /libraries

4. Create a directory for the MDBootstrap UI kit:
   * For MDBootstrap v5: /libraries/mdb-ui-kit
   * For MDBootstrap v4: /libraries/mdbootstrap"

5. Download 'Bootstrap framework' Desired Version:
   - https://github.com/twbs/bootstrap/tags

6. Place it in the /libraries/mdb-ui-kit directory i.e. Required files.
   * (or /libraries/mdbootstrap for MDBootstrap v4)

   * The path of the downloaded files (compiled CSS & JS) should be as follows:
     - /libraries/mdb-ui-kit/css/mdb.min.css
     - /libraries/mdb-ui-kit/js/mdb.umd.min.js

   * For MDBootstrap v4 should be as follows:
     - /libraries/mdbootstrap/css/mdb.min.css
     - /libraries/mdbootstrap/js/mdb.min.js

   * NOTE: If both library names, 'mdb-ui-kit' and 'mdbootstrap', exist in your
   /libraries path, 'mdb-ui-kit' (MDB v5) will be prioritized automatically,
   and the 'mdbootstrap' library will be ignored.

7. Finally, enable 'MDBootstrap' module.


How does it Work?
-----------------

1. Enable "MDBootstrap" module, Follow INSTALLATION in above.

2. Go to Bootstrap UI settings:
   - /admin/config/user-interface/bootstrap

3. Change "Library" option from Bootstrap to MDBootstrap, save configuration.

4. Adjust the default settings as needed. See USAGE below for instructions.

5. Enjoy that.

This module allows you to seamlessly and flexibly integrate the popular
Material Design for Bootstrap framework into your Drupal sites.


USAGE
=====

### Introduction to Using the Material Design Bootstrap Module for Drupal

This module is designed to simplify and enhance the experience of using
Material Design for Bootstrap with Drupal sites. With this module, you can
effortlessly leverage the power of MDBootstrap's UI kits, enjoying seamless
integration and a variety of management settings. Key features of this module:

- **Support for All Versions:**
    This module supports both the Material Design for Bootstrap v4.x and v5.x.
- **Dark Mode:** Enable Dark Mode in MDB-UI-KIT through the MDBootstrap
    module settings for a sleek, dark-themed site.
- **Loading Options:** If you prefer not to download and locally load the
    MDBootstrap library, you can utilize the CDN option.
- **Development Mode:** For developers and debugging purposes, the module
    allows the use of non-minimized (Development) versions in its settings.
- **Custom Settings:** You can configure restrictions for loading MDBootstrap
    files, specify the themes used, and set page paths according to your needs.
- **RTL Support:** To support right-to-left languages such as Persian and
    Arabic, ensure that one of these languages is enabled on your site.


Documentation
-------------
https://mdbootstrap.com/docs/standard/getting-started/


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt
